package com.google.codelabs.authentification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.TokenResponse;

import org.json.JSONException;

import static com.google.codelabs.authentification.MainApplication.LOG_TAG;

public class MainActivity extends AppCompatActivity {

  private static final String SHARED_PREFERENCES_NAME = "AuthStatePreference";
  private static final String AUTH_STATE = "AUTH_STATE";
  private static final String USED_INTENT = "USED_INTENT";

  MainApplication mainApplication;

  //registration status
  AuthState authState;

  AppCompatButton bttnAuthorize;
  AppCompatButton bttnSignOut;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    mainApplication = (MainApplication) getApplication();
    bttnAuthorize = (AppCompatButton) findViewById(R.id.authorize);
    bttnSignOut = (AppCompatButton) findViewById(R.id.signOut);

    enablePostAuthorizationFlows();

    bttnAuthorize.setOnClickListener(new AuthorizeListener());
  }

  @Override
  protected void onNewIntent(Intent intent) {
    checkIntent(intent);
  }

  private void checkIntent(Intent intent) {
    if (intent != null) {
      String action = intent.getAction();
      assert action != null;
      switch (action) {
        case "com.google.codelabs.appauth.HANDLE_AUTHORIZATION_RESPONSE":
          if (!intent.hasExtra(USED_INTENT)) {
            handleAuthorizationResponse(intent);
            intent.putExtra(USED_INTENT, true);
          }
          break;
      }
    }
  }

  @Override
  protected void onStart() {
    super.onStart();
    checkIntent(getIntent());
  }


  private void enablePostAuthorizationFlows() {
    authState = restoreAuthState();
    if (authState != null && authState.isAuthorized()) {
      if (bttnSignOut.getVisibility() == View.GONE) {
        bttnSignOut.setVisibility(View.VISIBLE);
        bttnSignOut.setOnClickListener(new SignOutListener(this));
      }
    } else {
      bttnSignOut.setVisibility(View.GONE);
    }
  }

    private void handleAuthorizationResponse(Intent intent) {
    AuthorizationResponse response = AuthorizationResponse.fromIntent(intent);
    AuthorizationException error = AuthorizationException.fromIntent(intent);
    final AuthState authState = new AuthState(response, error);

    if (response != null) {
      Log.i(LOG_TAG, String.format("Handled Authorization Response %s ", authState.toJsonString()));
      AuthorizationService service = new AuthorizationService(this);
      service.performTokenRequest(response.createTokenExchangeRequest(), new AuthorizationService.TokenResponseCallback() {

        @Override
        public void onTokenRequestCompleted(TokenResponse tokenResponse, AuthorizationException exception) {
          if (exception != null) {
            Log.w(LOG_TAG, "Token Exchange failed", exception);
          } else {
            if (tokenResponse != null) {
              authState.update(tokenResponse, exception);
              persistAuthState(authState);
              Log.i(LOG_TAG, String.format("Token Response [ Access Token: %s, ID Token: %s ]", tokenResponse.accessToken, tokenResponse.idToken));
            }
          }
        }
      });
    }
  }


  //save auth state
  private void persistAuthState(AuthState authState) {
    getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
        .putString(AUTH_STATE, authState.toJsonString())
        .apply();
    enablePostAuthorizationFlows();
  }


  //remove auth state
  private void clearAuthState() {
    getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        .edit()
        .remove(AUTH_STATE)
        .apply();
  }


  //restore auth state
  private AuthState restoreAuthState() {
    String jsonString = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        .getString(AUTH_STATE, null);
    if (!TextUtils.isEmpty(jsonString)) {
      try {
        return AuthState.fromJson(jsonString);
      }
        catch (JSONException ignored) {
      }
    }
    return null;
  }

  //authorization on our app
  public static class AuthorizeListener implements Button.OnClickListener {
    @Override
    public void onClick(View view) {
      AuthorizationServiceConfiguration serviceConfiguration = new AuthorizationServiceConfiguration(
          Uri.parse("https://accounts.google.com/o/oauth2/v2/auth"),
          Uri.parse("https://www.googleapis.com/oauth2/v4/token")
      );

      String clientId = "511828570984-fuprh0cm7665emlne3rnf9pk34kkn86s.apps.googleusercontent.com";
      Uri redirectUri = Uri.parse("com.google.codelabs.appauth:/oauth2callback");
      AuthorizationRequest.Builder builder = new AuthorizationRequest.Builder(
          serviceConfiguration,
          clientId,
          AuthorizationRequest.RESPONSE_TYPE_CODE,
          redirectUri
      );
      builder.setScopes("profile");
      AuthorizationRequest request = builder.build();

      AuthorizationService authorizationService = new AuthorizationService(view.getContext());

      String action = "com.google.codelabs.appauth.HANDLE_AUTHORIZATION_RESPONSE";
      Intent postAuthorizationIntent = new Intent(action);
      PendingIntent pendingIntent = PendingIntent.getActivity(view.getContext(), request.hashCode(), postAuthorizationIntent, 0);
      authorizationService.performAuthorizationRequest(request, pendingIntent);
    }
  }

  //sign out of the app
  public static class SignOutListener implements Button.OnClickListener {

    private final MainActivity mMainActivity;

    public SignOutListener(MainActivity mainActivity) {
      mMainActivity = mainActivity;
    }

    @Override
    public void onClick(View view) {
      mMainActivity.authState = null;
      mMainActivity.clearAuthState();
      mMainActivity.enablePostAuthorizationFlows();
    }
  }
}
